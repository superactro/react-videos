import axios from 'axios';

const KEY = 'AIzaSyB0DPztdWUBXjZPOkOwBxmOYTfKjjY36xE';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3/',
  params: {
    part: 'snippet',
    type: 'video',
    results: 5,
    key: KEY,
  },
});
